import OPi.GPIO as GPIO			#import library orange pi GPIO

import datetime					#library tangga & waktu
import time						#library waktu (ms)
import json
class Timer(object):			#kontruksi dari fungsi
    """A simple timer class"""
    def __init__(self):
        pass
    def start(self):			#fungsi start timer
        """Starts the timer"""
        self.start = datetime.datetime.now()
        return self.start
    def stop(self, message=""):					#fungsi stop timer
        """Stops the timer.  Returns the time elapsed"""
        self.stop = datetime.datetime.now()
        return str(self.stop - self.start)





def membuatJson(a,b):					#fungsi JSON
	c=str(datetime.datetime.now())
	payload={}
	payload['reason'] = str(a)
	payload['time_downtime'] = str(datetime.datetime.now())
	payload['duration'] = b
	return json.dumps(payload)


StatusDebug = True
#Adjust GPIO
GPIO.cleanup()
pinArray =[24,26]
ReasonArray=['24a','26a']
statusPressed=[False,False]
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(pinArray,GPIO.IN)
hadPressed = False
index=0
i=0
count=0
startTick = None

for x in pinArray:
	GPIO.add_event_detect(x, GPIO.BOTH, bouncetime=4000)  # add rising edge detection on a channel




while True:
	for x in pinArray:
		if GPIO.event_detected(x):
			#ReasonArray[index]=Timer()
			#ReasonArray[index].start()
			if GPIO.input(x) == GPIO.LOW:
				statusPressed[index] = True
				#i=membuatJson(x,"0")
				#print(i)
				count+=1
				d=str(count)
				print("Counter:"+d)
				# kirimDataKeServer(i)
		        if GPIO.input(x) == GPIO.HIGH and statusPressed[index]:
				#j=ReasonArray[index].stop()
				#k=membuatJson(x,j)
				#kirimDataKeServer(k)
				statusPressed[index]=False
				#print(k)

		index +=1
	count=count
	index=0
#	for x in pinArray:
#		GPIO.remove_event_detect(x)


print("finish")

