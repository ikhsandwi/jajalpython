import OPi.GPIO as GPIO      #library GPIO
import time             #library time to use sleep

GPIO.setmode(GPIO.BOARD)    #use board pin numbering
GPIO.setup(7,GPIO.OUT)      #setup gpio 7 to output

def Blink(numTimes,speed):
    for i in range(0,numTimes):
        print('Iteration'+str(i+1))
        GPIO.output(7,True)
        time.sleep(speed)
        GPIO.output(7,False)
        time.sleep(speed)
    print('Done')
    GPIO.cleanup()

iterations=raw_input('Enter times to blink: ')
speed=raw_input('Enter times speed blink: ')

Blink(int(iterations),float(speed))